# pull official base image
FROM python:3.7.10-slim-buster

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV FLASK_ENV=production
ENV DATABASE_URL=postgresql+psycopg2://sortan_admin:sortan_password012345@178.128.118.174:5433/sortan_production_db
ENV JWT_SECRET_KEY=hhgaghhgsdhdhdd

EXPOSE 5000

# copy project
COPY . /usr/src/app/

# update apt-get
# RUN apt-get update && apt-get install -y apt-transport-https
# RUN echo 'deb http://private-repo-1.hortonworks.com/HDP/ubuntu14/2.x/updates/2.4.2.0 HDP main' >> /etc/apt/sources.list.d/HDP.list
# RUN echo 'deb http://private-repo-1.hortonworks.com/HDP-UTILS-1.1.0.20/repos/ubuntu14 HDP-UTILS main'  >> /etc/apt/sources.list.d/HDP.list
# RUN echo 'deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/azurecore/ trusty main' >> /etc/apt/sources.list.d/azure-public-trusty.list
RUN apt-get update
RUN apt-get -y upgrade

# install ffmpeg
RUN apt-get install -y ffmpeg

RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt





# FROM python:3.8-alpine3.13
# # set work directory
# WORKDIR /usr/src/app

# # set environment variables
# ENV PYTHONDONTWRITEBYTECODE=1
# ENV PYTHONUNBUFFERED=1
# ENV FLASK_ENV=production
# ENV DATABASE_URL=postgresql+psycopg2://sortan_admin:sortan_password012345@localhost:5433/sortan_production_db
# ENV JWT_SECRET_KEY=hhgaghhgsdhdhdd

# # copy project
# COPY . /usr/src/app/

# # install dependencies
# RUN apk add --update ffmpeg

# # add c libraries, since alpine is a bitch and not using what other people use
# # RUN apk add --update-cache --no-cache libgcc libquadmath musl \
# # && apk add --update-cache --no-cache libgfortran \
# # && apk add --update-cache --no-cache lapack-dev

# RUN pip3 install --upgrade pip
# RUN pip3 install -r requirements.txt





# pull official base image
# FROM ubuntu:20.04

# # set work directory
# WORKDIR /usr/src/app

# # set environment variables
# ENV PYTHONDONTWRITEBYTECODE=1
# ENV PYTHONUNBUFFERED=1
# ENV FLASK_ENV=production
# ENV DATABASE_URL=postgresql+psycopg2://sortan_admin:sortan_password012345@localhost:5433/sortan_production_db
# ENV JWT_SECRET_KEY=hhgaghhgsdhdhdd

# # copy project
# COPY . /usr/src/app/

# # update apt-get
# RUN apt-get update
# RUN apt-get -y upgrade

# # install ffmpeg
# RUN apt-get install -y ffmpeg

# # install python
# RUN apt-get install -y python3.8 python3.8-pip

# RUN pip3 install --upgrade pip
# RUN pip3 install -r requirements.txt
