## This is the main implemation of Sortan server.
### enviroments, versions, requirements:
This implementation is using `python 3.7` with the dependancies listed in `requirements.txt`. Database will be using `Postgresql` version `13.2` however version `11` also work fine. And in other to run please create a database using `createdb sortan_db`.

To initialize the migration input this command into the terminal `python3 manage.py db init` and to run migration please run `python3 manage.py db migrate` then \
`python3 manage.py db upgrade`.
### running tests:
to run test, please use `pytest --cov=src`
### editors and extensions:
Currently I (sovichea) using `vscode` for coding editing with the official python extension. However, there is an error with `REPL` where the import isn't recognize. This can be fix by adding and `.someEnvFile` containing **PYTHONPATH=./** and in `.vscode/settings.json` add **"python.envFile": "${workspaceFolder}/.curDirEnv"** to the json object.
### enviroments:
I am using `anaconda` virtual enviroment, but anything can works including `venv` and `pipenv`.
### directory layout:
```
root
|-- .gitignore
|-- manage.py
|-- run.py
|-- readme.md
|-- requirements.txt
|-- set_env.sh
|-- src
    |-- __init__.py
    |-- shared
        |-- __init__.py
        |-- HelpersFunctions.py
    |-- models
        |-- __init__.py
        |-- ExampleModel.py
    |-- views.py
        |-- __init__.py
        |-- ExampleView.py
    |-- app.py
    |-- config.md
```
- manage.py: is the migration management for the database.
- run.py: is the starting endpoint, to start is server simply run `python3 run.py`
- readme.md: is this document for explaining how the project is structure and how to run it on your local machine.
- requirements.txt: will list the dependency for this projects.
- set_env.sh: contain the `enviroment variables` that need to for run the server. (this is for ease of testing only. `.env` file should kept as a secret and should not be shown in the git repo)
- src: is the directory that contain the sever source code.
- \_\_init\_\_.py: for \_\_init\_\_.py is like a `.h` file in `C/C++`. It purpose is for collecting `import` in that directory and can allow other file to import either in the same directory or for outsider.
- src/share: is a directory to store helpers function that can be use anywhere.
- src/models: is the directory to store data `model` and `schema` and functions that manipulate the database table that have the same name as the model.
- src/views: is the directory that store files that create `REST api` endpionts.
### errors raising convension:
- `Validation Error`: will be raise for Schema loading and dumping.
- `Exception`: will be raise for database call or api call.
- `Runtime Error`: will be raise for when any other functions, either being a helper or help validating data before calling database api (for self define function).


### seeding:
https://stackoverflow.com/questions/19334604/creating-seed-data-in-a-flask-migrate-or-alembic-migration

### hosting
- https://www.digitalocean.com/community/tutorials/how-to-set-up-continuous-integration-pipelines-with-gitlab-ci-on-ubuntu-16-04
- https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx/
- https://realpython.com/dockerizing-flask-with-compose-and-machine-from-localhost-to-the-cloud/
- https://medium.com/@huseinzolkepli/how-to-ci-cd-flask-app-using-gitlab-ci-4297017acda1
- https://gitlab.bio.di.uminho.pt/tutorials/python-flask-docker-ci-helloworld-example

`But will have to use this one:`
- https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx/
- https://stackoverflow.com/questions/55243023/docker-compose-postgresql-create-db-user-pass-and-grant-permission

`Run directly on the server`
- https://janakiev.com/blog/python-background/

### sqlalchemy data relationship:
- https://docs.sqlalchemy.org/en/14/orm/cascades.html
- https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html
- https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/
- https://stackoverflow.com/questions/5033547/sqlalchemy-cascade-delete
- https://stackoverflow.com/questions/24970594/delete-one-to-one-relationship-in-flask
- https://stackoverflow.com/questions/51335298/concepts-of-backref-and-back-populate-in-sqlalchemy

### many to many query
- https://stackoverflow.com/questions/40699642/how-to-query-many-to-many-sqlalchemy

### searching refferences:
- how-to-get-rows-which-match-a-list-of-3-tuples-conditions-with-sqlalchemy: https://stackoverflow.com/questions/9140015/how-to-get-rows-which-match-a-list-of-3-tuples-conditions-with-sqlalchemy
- sql count: https://docs.sqlalchemy.org/en/14/orm/tutorial.html#counting
- filter count column: https://stackoverflow.com/questions/6244038/sqlalchemy-filter-by-count-column
- context to previous link about subquery: https://stackoverflow.com/questions/34446725/sqlalchemy-func-count-with-filter
- 3rd answer: https://stackoverflow.com/questions/12941416/how-to-count-rows-with-select-count-with-sqlalchemy
- use having: https://docs.sqlalchemy.org/en/14/core/tutorial.html#ordering-grouping-limiting-offset-ing
### filter pandas:
- https://stackoverflow.com/questions/29836836/how-do-i-filter-a-pandas-dataframe-based-on-value-counts
