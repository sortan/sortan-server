import os
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from src.app import create_app, db

env_name = os.getenv('FLASK_ENV')
app = create_app(env_name)

migrate = Migrate(app=app, db=db)
# migrate = Migrate(app=app, db=db, compare_type=True)

manager = Manager(app=app)

manager.add_command('db', MigrateCommand)

@manager.command
def create_db():
  db.create_all()
  db.session.commit()

@manager.command
def drop_alembic_version():
  db.session.execute('DROP TABLE alembic_version;')

if __name__ == '__main__':
  manager.run()
