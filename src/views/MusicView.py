import os
import datetime
import time
import statistics
import threading

from youtube_dl import YoutubeDL
import pandas as pd
import numpy as np
from flask import g, request, json, Response, Blueprint
from marshmallow.exceptions import ValidationError

from ..models.MusicModel import MusicModel, MusicSchema
from ..models.FileModel import FileModel, FileSchema, file_get_full_path
from ..models.ArtistModel import ArtistModel, ArtistSchema
from ..models.FingerprintModel import FingerprintModel, FingerprintSchema
from ..models.MusicLinkModel import MusicLinkModel, MusicLinkSchema
from ..models import UPLOAD_FOLDER

from ..shared.static_variable import (
  HttpCode,
  OVERLAP, 
  WINDOW_SIZE, 
  LISTENABLE_RATE,
  VALID_MAX_FREQUENCY,
  VALID_SAMPLE_RATE
)
from ..shared.http_helpers import response_helper, http_msg_constructor

from ..shared.audio_helpers import (
  load_wav,
  is_audio_mono,
  get_wav_info,
  is_sample_rate_valid,
  generate_fingerprints,
  generate_searchable_data,
  stereo_to_mono,
  low_pass_filter,
  downsample
)

music_api = Blueprint('musics', __name__)
music_schema = MusicSchema()
music_link_schema = MusicLinkSchema()

@music_api.route('', methods=['POST'])
def create():
  '''
    get file id and extract the audio detail. Is it a wav? \
    is it a mono? is it downsampled? if not return error, else generate fingerprint.\n
    `Parameters:`\n
      - file-id: int
      - name: str
      - origin: str \n
    `Return:`\n
    \t Http Response
    '''
  request_data = request.get_json()
  file_schema = FileSchema()
  try:
    # get the file by id
    file_id = request_data.get('file_id')
    file_db = FileModel.get_a_file_by_id(file_id)
    file_obj = file_schema.dump(file_db)
    file_path = file_get_full_path(file_obj)
    if (not os.path.exists(file_path)):
      # sth really fuckup, delete the file and move on.
      file_db.delete()
      raise RuntimeError('Error, file does not exist watafak!')
    if (file_path.split('.')[-1] != 'wav'):
      # this is also fuck up, it should be wav, this can occur when dump dump who
      # implemented this forgot to remove the test file like `jpg` or `png`.
      file_db.delete()
      raise RuntimeError('Error, file should be wav!')
  except Exception as err:
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  # now load the path to using pydub and do some validations.
  # however as of now there is no way to detect spectral leakage.
  wav = load_wav(file_path)

  if not is_audio_mono(wav):
    file_db.delete()
    return response_helper(http_msg_constructor('Audio is not mono!'), HttpCode.BAD_REQUEST)

  samples, sample_rate = get_wav_info(wav)
  if not is_sample_rate_valid(sample_rate):
    file_db.delete()
    return response_helper(http_msg_constructor(f'The sample rate: {sample_rate} is not valid!'), HttpCode.BAD_REQUEST)

  # get the audio length in second.
  duration = wav.duration_seconds

  
  # validiate data and insert music collection.
  try:
    data = music_schema.load({
      'file_id': file_id,
      'name': request_data.get('name'),
      'origin': request_data.get('origin'),
      'duration': duration
    })
    music_db = MusicModel(data)
    music_db.save()
  except Exception as err:
    # file_db.delete()
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.BAD_REQUEST
    )
  
  # add music to artists that responsible.
  artists_id = request_data.get('artists_id')
  try:
    for artist_id in artists_id:
      artist_db = ArtistModel.get_an_artist_by_id(artist_id)
      artist_db.musics.append(music_db)
    artist_db.save()
      
    # deserilize music_db.
    music_obj = music_schema.dump(music_db)

    # add link to the music.
    for url in request_data.get('url'):
      link = MusicLinkModel(music_link_schema.load({
        'url': url,
        'music_id': music_obj['id']
      }))
      link.save()
  except Exception as err:
    # file_db.delete()
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.BAD_REQUEST
    )
  
  try:
    # generate the fingerprint.
    fingerprints = generate_fingerprints(
      samples, 
      sample_rate, 
      WINDOW_SIZE, 
      OVERLAP, 
      music_obj['id']
    )
    # save the fingerprints.
    FingerprintModel.save_all(fingerprints)
  except Exception as err:
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )

  return response_helper(
    http_msg_constructor(music_obj, True),
    HttpCode.CREATED
  )

@music_api.route('/search', methods=['POST'])
def search():
  start_time = time.time()

  # get the audio file.
  if 'file' not in request.files:
    return response_helper(
      http_msg_constructor('file not found!'),
      HttpCode.BAD_REQUEST
    )

  file_uploaded = request.files['file']

  if file_uploaded.filename == '':
    return response_helper(
      http_msg_constructor('file does not have a name!'),
      HttpCode.BAD_REQUEST
    )
  
  if not FileModel.is_file_supported(file_uploaded.filename):
    return response_helper(
      http_msg_constructor('file extension is not supported!'),
      HttpCode.BAD_REQUEST
    )

  try:
    filename = f"{datetime.datetime.now().strftime('%s')}-{file_uploaded.filename}"
    file_path = os.path.join(
      UPLOAD_FOLDER,
      filename
    )
    file_uploaded.save(file_path)

    wav = load_wav(file_path)

    if not is_audio_mono(wav):
      wav = stereo_to_mono(wav)

    _, sample_rate = get_wav_info(wav)
    if sample_rate != VALID_SAMPLE_RATE:
      raise RuntimeError(f'frequency rate is not {VALID_SAMPLE_RATE}!')

    # downsample and lowpass filter the audio first
    # wav = low_pass_filter(wav, VALID_MAX_FREQUENCY)
    # wav = downsample(wav, VALID_SAMPLE_RATE)
    samples, sample_rate = get_wav_info(wav)
    
    # addresses: [(anch_freq, point_freq, time_difference)...]
    # anchor_points: [(anch_freq, anch_time)...]
    # addresses, anchor_times, total_target_zones, lookup_ids = generate_searchable_data(samples, sample_rate, WINDOW_SIZE, OVERLAP)
    anchor_times, total_target_zones, lookup_ids = generate_searchable_data(samples, sample_rate, WINDOW_SIZE, OVERLAP)
    # fingerprint_schema = FingerprintSchema(only=('music_id', 'lookup_id', 'anchor_time'))
    fingerprints = FingerprintModel.search(lookup_ids)
    # fingerprints = fingerprint_schema.dump(fingerprints, many=True) # .items
    # count the anchor_frequency appear. this will determine the 
    # filter any fingerprint that did not form target zone.
    # (if couple (music_id, anchor_freq, anchor_time) > 4 pass it)
    # the paper only filter with this (music_id, anchor_time).
    # fingerprints = pd.DataFrame(fingerprints)
    '''
    does not seem to work as well as the last line.
    # fingerprints = fingerprints.groupby(['music_id', 'anchor_frequency', 'anchor_time']).filter(lambda anchor_time: len(anchor_time) >= 4)
    # fingerprints = fingerprints.groupby(['music_id', 'anchor_frequency', 'point_frequency', 'time_difference']).filter(lambda anchor_time: len(anchor_time) >= 4)
    # fingerprints = fingerprints.groupby(['music_id', 'anchor_frequency', 'anchor_time']).filter(lambda anchor_time: len(anchor_time) >= 4)
    this what the paper use:
    # fingerprints = fingerprints.groupby(['music_id', 'anchor_time']).filter(lambda anchor_time: len(anchor_time) >= 4)
    '''

    
    # this slow:
    # fingerprints = fingerprints.groupby(['music_id', 'anchor_time']).filter(lambda anchor_time: len(anchor_time) >= 4)
    # use this instead
    fingerprints = fingerprints[fingerprints.groupby(['music_id', 'anchor_time'])['anchor_time'].transform('count').gt(3)]
    
    # split the fingerprints by music_id.
    # get unique music_id.
    music_ids = fingerprints.music_id.unique()
    # group the damn fingerprints by music_id.
    fingerprints = fingerprints.groupby('music_id')
    # split the fingerprints df by music id and filter any music that did not

    '''
    This was a flock, it should be 

    # The threshold for filtering.
    MAX_FINGERPRINT_THRESHOLD = 0.8
    # produce at least 80% of the recorded targetzone.
    musics_df = []
    for music_id in music_ids:
      music_df = fingerprints.get_group(music_id)
      if music_df.size >= total_target_zones * MAX_FINGERPRINT_THRESHOLD:
        musics_df.append(music_df.sort_values('point_time'))
        # musics_df.append(music_df)
    '''

    
    # this versio is too strict.
    MAX_FINGERPRINT_THRESHOLD = 1
    # produce at least 80% of the recorded targetzone.
    musics_df = []
    for music_id in music_ids:
      music_df = fingerprints.get_group(music_id)
      if music_df.shape[0] >= total_target_zones * MAX_FINGERPRINT_THRESHOLD:
        musics_df.append(music_df.sort_values('anchor_time'))
        # musics_df.append(music_df.sort_values('point_time'))
        # musics_df.append(music_df)
    
    

    '''
    # Another way to filter possible songs.
    CUT_OFF_LIMIT = 3
    # group the music by id and count the fingerprint of each music_id.
    musics_df = tuple(map(
      lambda music_id: (fingerprints.get_group(music_id).shape[0], fingerprints.get_group(music_id)) ,
      music_ids
    ))
    # get the top 3 music with the most match fingerprints (3 is a parameter can change)
    musics_df = tuple(
      map(lambda x: x[1], sorted(musics_df, key=lambda tup: tup[0], reverse=True)[:CUT_OFF_LIMIT])
    )
    '''
    

    # if music_df == 0 return sorry.
    if len(musics_df) == 0:
      os.remove(file_path)
      return response_helper(
        http_msg_constructor('Sorry, no music found!'),
        HttpCode.BAD_REQUEST
      )
    
    '''
    calculate the time Coherence and rearrange to find the matched song.
    first for music data frame, we subtract the the recorded anchor_time
    on the matching address and store them into a list. Then we find the
    most appear delta time in that said list and store the count value
    along with the music id in to another list:
    [{ music_id, occue_time }...]
    '''

    # using multi-threading.
    anchor_time_diff_music_id_list = []
    
    '''
    too slow
    def run_time_occur_diff_per_song(music_df):
      anchor_time_diff_max_occur = []
      for i, anchor_time in enumerate(anchor_times):
        anchor_time_diff_max_occur.extend(
          music_df.loc[
            (music_df['anchor_frequency'] == addresses[i][0]) &
            (music_df['point_frequency'] == addresses[i][1]) &
            (music_df['time_difference'] == addresses[i][2])
        ]['anchor_time'].subtract(anchor_time).tolist())

      anchor_time_diff_music_id_list.append({
        'music_id': music_df['music_id'].tolist()[0],
        'anchor_time_diff_max_occur': pd.DataFrame(anchor_time_diff_max_occur).value_counts().max()
      })
    '''

    # try to improve run_time_occur_diff_per_song()
    data = {
      'anchor_time': anchor_times,
      'lookup_id': lookup_ids
    }
    anchor_time_df = pd.DataFrame(data=data)
    def run_time_occur_diff_per_song(music_df, anchor_time_df):
      anchor_time_diff_max_occur = pd.merge(
        music_df.rename(columns={'anchor_time': 'anchor_time1'}),
        anchor_time_df.rename(columns={'anchor_time': 'anchor_time2'}), how='outer'
      ).eval('change = anchor_time1 - anchor_time2')['change'].value_counts().max()

      anchor_time_diff_music_id_list.append({
        'music_id': music_df['music_id'].tolist()[0],
        'anchor_time_diff_max_occur': anchor_time_diff_max_occur
      })
      
    thread_pool = []
    for music_df in musics_df:
      thread = threading.Thread(target=run_time_occur_diff_per_song, args=(music_df, anchor_time_df,))
      thread_pool.append(thread)
      thread.start()
    
    for thread in thread_pool:
      thread.join()

    '''
    anchor_time_diff_music_id_list = []
    for music_df in musics_df:
      anchor_time_diff_music_id_list.append({
        'music_id': music_df['music_id'].tolist()[0],
        # 'anchor_time_diff_max_occur': music_df['point_time'].diff().value_counts().max()
        'anchor_time_diff_max_occur': np.sum(music_df['anchor_time'].diff() == 1)
      })
    '''
    

    # then compare with other music and select the one with the highest delta_time_count value
    # will get it id return.
    result_music_dict = max(anchor_time_diff_music_id_list, key=lambda x: x['anchor_time_diff_max_occur'])
    music_id = result_music_dict['music_id']
    music_db = MusicModel.get_a_music_by_id(music_id)
    music_obj = music_schema.dump(music_db)

    os.remove(file_path)
    end_time = time.time()
    delta_time = end_time - start_time
    music_obj['time'] = delta_time
    print('time to compute is: ', delta_time)
    print('highest score: ', result_music_dict['anchor_time_diff_max_occur'])
    return response_helper(
      http_msg_constructor(music_obj, True),
      HttpCode.OK
    )

  except Exception as err:
    os.remove(file_path)
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.BAD_REQUEST
    )
  
@music_api.route('/populate', methods=['POST'])
def populate():
  # load schema and model
  file_schema = FileSchema()
  artist_schema = ArtistSchema()
  # download.
  CUR_PATH = os.path.join(os.getcwd(), 'files/upload')
  ydl_opts = {
    'noplaylist': True,
    'extract-audio': True,
    'audio-format': 'wav',
    # 'format': 'm4a',
    'postprocessors': [{
      'key': 'FFmpegExtractAudio',
      'preferredcodec': 'wav',
      'preferredquality': '192',
    }],
    'outtmpl': CUR_PATH + '/%(title)s|||%(uploader)s.%(ext)s',
    # 'outtmpl': CUR_PATH + '/%(title)s_%(uploader)s.mp3',
  }

  try:
    with YoutubeDL(ydl_opts) as ydl:
      request_data = request.get_json()
      playlist = request_data.get('playlist')
      if not playlist:
        raise RuntimeError('No playlist found!')
      urls_dict = {}
      info = ydl.extract_info(playlist, download=True)
      print('Finish the download.')
      for entry in info['entries']:
        urls_dict[entry['title']] = entry['webpage_url']
      print('Loading the url to a dictionary.')

    # go through each file and create the db record.
    for file_path in os.listdir(CUR_PATH):
      if FileModel.is_file_supported(file_path):
        full_path = os.path.join(CUR_PATH, file_path)
        size = os.path.getsize(full_path) 
        data = file_schema.load({
          "size": size,
          "path": full_path,
          "type": 'audio/wav'
        })
        file_db = FileModel(data)
        file_db.save()
        file_obj = file_schema.dump(file_db)
        print('Save file record to db.')

        # create music record.
        wav = load_wav(full_path)
        # downsample and change channel.
        wav = stereo_to_mono(wav)
        wav = low_pass_filter(wav, VALID_MAX_FREQUENCY)
        wav = downsample(wav, VALID_SAMPLE_RATE)
        print('Finish downsample.')

        # get the music details.
        [video_title, video_uploader] = file_path.split('|||')
        video_uploader = video_uploader.replace('.wav', '')

        # create the the music record.
        data = music_schema.load({
          'file_id': file_obj['id'],
          'name': video_title,
          'origin': 'Khmer',
          'duration': wav.duration_seconds,
        })
        music_db = MusicModel(data)
        music_db.save()
        music_obj = music_schema.dump(music_db)
        print('Save music record to db.')

        # get artist if there is none create one.
        artist = ArtistModel.get_an_artist_by_name(video_uploader)
        if artist == None:
          artist = ArtistModel(artist_schema.load({
            'name': video_uploader
          }))
        artist.musics.append(music_db)
        artist.save()
        print('Save artist record to db.')

        # create the url record.
        link = MusicLinkModel(music_link_schema.load({
          'url': urls_dict[video_title],
          'music_id': music_obj['id']
        }))
        link.save()
        print('Save link record to db.')

        # generate fingerprints.
        # generate the fingerprint.
        samples, sample_rate = get_wav_info(wav)
        fingerprints = generate_fingerprints(
          samples, 
          sample_rate, 
          WINDOW_SIZE, 
          OVERLAP, 
          music_obj['id']
        )
        # save the fingerprints.
        FingerprintModel.save_all(fingerprints)
        print('Save all fingerprints record to db.')

        os.remove(full_path)
        print('Remove the filr from file system.')
  except Exception as err:
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor('Musics, Artists, and Fingerprints have been created', True),
    HttpCode.CREATED
  )

  

@music_api.route('/<int:music_id>', methods=['DELETE'])
def delete_a_music_by_id(music_id):
  try:
    music_db = MusicModel.get_a_music_by_id(music_id)
    music_db.delete()
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor("music have been deleted", True),
    HttpCode.OK
  )
