from flask import g, request, json, Response, Blueprint
from ..models.UserModel import UserModel, UserSchema
from marshmallow.exceptions import ValidationError

from ..shared.authenticate import Auth

from ..shared.static_variable import HttpCode
from ..shared.http_helpers import response_helper, http_msg_constructor

user_api = Blueprint('users', __name__)
user_schema = UserSchema()

@user_api.route('', methods=['POST'])
# @Auth.authenticate
def create():
  '''
  create user route with: /users/
  '''
  # load data from body.
  request_data = request.get_json()
  # load data to schema
  try:
    data = user_schema.load(request_data)
  except ValidationError as err:
    return response_helper(http_msg_constructor(str(err)), HttpCode.BAD_REQUEST)
  
  # check if the user is in the db already.
  try:
    user_in_db = UserModel.get_a_user_by_email(data.get('email'))
    if user_in_db:
      res = { 'message': 'User already exist in the database' }
      return response_helper(res, HttpCode.BAD_REQUEST)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(http_msg_constructor(str(err)), HttpCode.INTERNAL_SERVER_ERROR)
  
  # create the User.
  try:
    user = UserModel(data)
    user.save()
    # serialize the data back, excluding the password.
    user_no_pwd_schema = UserSchema(exclude=('password'))
    user_obj = user_no_pwd_schema.dump(user)
    # return the user.
    return response_helper(
      http_msg_constructor(user_obj, True),
      HttpCode.CREATED
    )
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )

@user_api.route('/login', methods=['POST'])
def login():
  '''
  create user route with: /users/login/
  by comparing passord form the user with the requested
  email and the requested password.\n
  `Returns:`\n
  user details and a jwt auth token.
  '''
  # get data.
  req_data = request.get_json()
  # validate data.
  try:
    data = user_schema.load(req_data, partial=True)
  except ValidationError as err:
    return response_helper(http_msg_constructor(str(err)), HttpCode.BAD_REQUEST)
    
  # get email and password from request body.
  email, password = data.get('email'), data.get('password')
  if not email or not password:
    return response_helper(http_msg_constructor('Email or password is missing!'), HttpCode.BAD_REQUEST)

  try:
    # get the user by email.
    db_user = UserModel.get_a_user_by_email(email)
    if not db_user:
      response_helper({'message': 'User not found!'}, HttpCode.BAD_REQUEST)
    # compare req password and user password.
    if not db_user.compare_password(password):
      response_helper({'message': 'Incorrect Password!'}, HttpCode.BAD_REQUEST)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(http_msg_constructor(str(err)), HttpCode.INTERNAL_SERVER_ERROR)
  
  try:
    # dump user data back to dict without password.
    user_no_pwd_schema = UserSchema(exclude=('password'))
    user_obj = user_no_pwd_schema.dump(db_user)
    # add the auth token to user_obj.
    # user_obj.update({ 'auth_token':  })
  except ValidationError as err:
    return response_helper(http_msg_constructor(str(err)), HttpCode.BAD_REQUEST)

  # return the final result.
  return response_helper(
    http_msg_constructor(user_obj, True),
    HttpCode.OK
  )
