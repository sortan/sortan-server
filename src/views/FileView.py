import os
import datetime
from flask import g, request, json, Response, Blueprint
from marshmallow.exceptions import ValidationError

from ..models.FileModel import FileModel, FileSchema
from ..models import ALLOW_FILE_EXTENSION, UPLOAD_FOLDER

from ..shared.static_variable import HttpCode
from ..shared.http_helpers import response_helper, http_msg_constructor

file_api = Blueprint('files', __name__)
file_schema = FileSchema()

@file_api.route('', methods=['POST'])
def create():
  if 'file' not in request.files:
    return response_helper(
      http_msg_constructor('file not found!'),
      HttpCode.BAD_REQUEST
    )

  file_uploaded = request.files['file']

  if file_uploaded.filename == '':
    return response_helper(
      http_msg_constructor('file does not have a name!'),
      HttpCode.BAD_REQUEST
    )
  
  if not FileModel.is_file_supported(file_uploaded.filename):
    return response_helper(
      http_msg_constructor('file extension is not supported!'),
      HttpCode.BAD_REQUEST
    )

  # select the require data from upload file
  # set the pointer to end, read the data size, set the pointer back to origin.
  file_uploaded.seek(0, os.SEEK_END)
  size = file_uploaded.tell()
  file_uploaded.seek(0, os.SEEK_SET)
  try:
    filename = f"{datetime.datetime.now().strftime('%s')}-{file_uploaded.filename}"
    data = file_schema.load({
      "size": size,
      "path": filename,
      "type": file_uploaded.mimetype
    })
    file_db = FileModel(data)
    file_db.save()
    file_uploaded.save(os.path.join(
      UPLOAD_FOLDER,
      filename
    ))
    file_obj = file_schema.dump(file_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )

  return response_helper(
    http_msg_constructor(file_obj, True),
    HttpCode.OK
  )

@file_api.route('', methods=['GET'])
def get():
  # check for existing query string exist.
  keywords = request.args.get('keywords')
  page = request.args.get('page')
  per_page = request.args.get('per_page')
  '''order_by = request.args.get('order_by')
  direction = request.args.get('direction')'''

  # add default value to search option
  page =int(page) if page != None else 1
  per_page = int(per_page) if per_page != None else 10
  '''order_by = order_by if order_by != None else 'created_at'
  direction = direction if direction == 'asc' else 'desc'''

  try:
    if keywords == None:
      files = FileModel.get_pagination(page, per_page)
    else:
      files = FileModel.fuzzy_search(page, per_page, keywords)

    files_obj = file_schema.dump(files.items, many=True)
    return response_helper(
      http_msg_constructor(files_obj, True),
      HttpCode.OK
    )
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR)
  
@file_api.route('/<int:file_id>', methods=['GET'])
def get_a_file_by_id(file_id):
  try:
    file_db = FileModel.get_a_file_by_id(file_id)
    file_obj = file_schema.dump(file_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(file_obj, HttpCode.OK)

@file_api.route('/<int:file_id>', methods=['DELETE'])
def delete_a_file_by_id(file_id):
  try:
    file_db = FileModel.get_a_file_by_id(file_id)
    file_db.delete()
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor("file have been deleted", True),
    HttpCode.OK
  )

@file_api.route('/test_upload', methods=['POST'])
def test_upload():
  if 'file' not in request.files:
    return response_helper(
      http_msg_constructor('file not found!'),
      HttpCode.BAD_REQUEST
    )

  file_uploaded = request.files['file']

  if file_uploaded.filename == '':
    return response_helper(
      http_msg_constructor('file does not have a name!'),
      HttpCode.BAD_REQUEST
    )
  
  if not FileModel.is_file_supported(file_uploaded.filename):
    return response_helper(
      http_msg_constructor('file extension is not supported!'),
      HttpCode.BAD_REQUEST
    )

  return response_helper(
    http_msg_constructor('test success', True),
    HttpCode.OK
  )
