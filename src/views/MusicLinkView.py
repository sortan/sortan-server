import os
import datetime
from flask import g, request, json, Response, Blueprint
from marshmallow.exceptions import ValidationError

from ..models.MusicLinkModel import MusicLinkModel, MusicLinkSchema

from ..shared.static_variable import HttpCode
from ..shared.http_helpers import response_helper, http_msg_constructor

music_links_api = Blueprint('music_links', __name__)
music_links_schema = MusicLinkSchema()

@music_links_api.route('', methods=['POST'])
def create():
  request_data = request.get_json()
  try:
    data = music_links_schema.load({
      'url': request_data.get('url'),
      'music_id': request_data.get('music_id')
    })
    link_db = MusicLinkModel(data)
    link_db.save()
    link_obj = music_links_schema.dump(link_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )

  return response_helper(
    http_msg_constructor(link_obj, True),
    HttpCode.OK
  )

@music_links_api.route('', methods=['GET'])
def get():
  # check for existing query string exist.
  keywords = request.args.get('keywords')
  page = request.args.get('page')
  per_page = request.args.get('per_page')
  '''order_by = request.args.get('order_by')
  direction = request.args.get('direction')'''

  # add default value to search option
  page =int(page) if page != None else 1
  per_page = int(per_page) if per_page != None else 10
  '''order_by = order_by if order_by != None else 'created_at'
  direction = direction if direction == 'asc' else 'desc'''

  try:
    if keywords == None:
      link_db = MusicLinkModel.get_pagination(page, per_page)
    else:
      link_db = MusicLinkModel.fuzzy_search(page, per_page, keywords)

    link_obj = music_links_schema.dump(link_db.items, many=True)
    return response_helper(
      http_msg_constructor(link_obj, True),
      HttpCode.OK
    )
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR)
  
@music_links_api.route('/<int:link_id>', methods=['GET'])
def get_an_link_by_id(link_id):
  try:
    link_db = MusicLinkModel.get_an_link_by_id(link_id)
    link_obj = music_links_schema.dump(link_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor(link_obj, True),
    HttpCode.OK
  )

@music_links_api.route('/<int:link_id>', methods=['PUT'])
def update_an_link_by_id(link_id):
  request_data = request.get_json()
  try:
    data = music_links_schema.load(request_data)
    link_db = MusicLinkModel.get_an_link_by_id(link_id)
    link_db.update(data)
    link_obj = music_links_schema.dump(link_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor(link_obj, True),
    HttpCode.OK
  )

@music_links_api.route('/<int:link_id>', methods=['DELETE'])
def delete_an_link_by_id(link_id):
  try:
    link_db = MusicLinkModel.get_an_link_by_id(link_id)
    link_db.delete()
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor("file have been deleted", True),
    HttpCode.OK
  )
