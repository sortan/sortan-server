import os
import datetime
from flask import g, request, json, Response, Blueprint
from marshmallow.exceptions import ValidationError

from ..models.ArtistModel import ArtistModel, ArtistSchema

from ..shared.static_variable import HttpCode
from ..shared.http_helpers import response_helper, http_msg_constructor

artist_api = Blueprint('artists', __name__)
artist_schema = ArtistSchema()

@artist_api.route('', methods=['POST'])
def create():
  request_data = request.get_json()
  try:
    data = artist_schema.load({'name': request_data.get('name')})
    artist_db = ArtistModel(data)
    artist_db.save()
    artist_obj = artist_schema.dump(artist_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )

  return response_helper(
    http_msg_constructor(artist_obj, True),
    HttpCode.CREATED
  )

@artist_api.route('', methods=['GET'])
def get():
  # check for existing query string exist.
  keywords = request.args.get('keywords')
  page = request.args.get('page')
  per_page = request.args.get('per_page')
  '''order_by = request.args.get('order_by')
  direction = request.args.get('direction')'''

  # add default value to search option
  page =int(page) if page != None else 1
  per_page = int(per_page) if per_page != None else 10
  '''order_by = order_by if order_by != None else 'created_at'
  direction = direction if direction == 'asc' else 'desc'''

  try:
    if keywords == None:
      artists = ArtistModel.get_pagination(page, per_page)
    else:
      artists = ArtistModel.fuzzy_search(page, per_page, keywords)

    artists_obj = artist_schema.dump(artists.items, many=True)
    return response_helper(
      http_msg_constructor(artists_obj, True),
      HttpCode.OK
    )
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR)
  
@artist_api.route('/<int:artist_id>', methods=['GET'])
def get_an_artist_by_id(artist_id):
  try:
    artist_db = ArtistModel.get_an_artist_by_id(artist_id)
    artist_obj = artist_schema.dump(artist_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor(artist_obj, True),
    HttpCode.OK
  )

@artist_api.route('/<int:artist_id>', methods=['PUT'])
def update_an_artist_by_id(artist_id):
  request_data = request.get_json()
  try:
    data = artist_schema.load(request_data)
    artist_db = ArtistModel.get_an_artist_by_id(artist_id)
    artist_db.update(data)
    artist_obj = artist_schema.dump(artist_db)
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor(artist_obj, True),
    HttpCode.OK
  )

@artist_api.route('/<int:artist_id>', methods=['DELETE'])
def delete_an_artist_by_id(artist_id):
  try:
    artist_db = ArtistModel.get_an_artist_by_id(artist_id)
    artist_db.delete()
  except Exception as err:
    # check for unexpected errors.
    return response_helper(
      http_msg_constructor(str(err)),
      HttpCode.INTERNAL_SERVER_ERROR
    )
  
  return response_helper(
    http_msg_constructor("file have been deleted", True),
    HttpCode.OK
  )
