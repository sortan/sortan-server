import datetime
from typing import Union

from marshmallow import fields, Schema, validate

from . import db, bcrypt
from ..shared.static_variable import HttpCode
from ..shared.http_helpers import http_error_msg

CHAR_LIMIT = 256

class MusicModel(db.Model):
  '''
  Music model.
  '''

  # assign table name.
  __tablename__ = 'musics'

  # music table's fields.
  id = db.Column(db.Integer, primary_key=True)
  file_id = db.Column(db.Integer, db.ForeignKey('files.id'), unique=True, nullable=False)
  fingerprints = db.relationship('FingerprintModel', backref='music', uselist=True, lazy=True, cascade="all,delete,delete-orphan")
  links = db.relationship('MusicLinkModel', backref='music', uselist=True, lazy=True, cascade="all,delete,delete-orphan")

  duration = db.Column(db.Float, nullable=False)
  name = db.Column(db.String(CHAR_LIMIT), nullable=False)
  origin = db.Column(db.String(CHAR_LIMIT), nullable=False)
  created_at = db.Column(db.DateTime)
  modified_at = db.Column(db.DateTime)

  def __init__(self, data):
    '''
    create a music ORM.\n
    `Parameters:`\n
    \tself (MusicModel): The current music obj.
    \tdata (dict): dictionary of data need to create a music.
    `Return:`\n
    \t None: The function will create an ORM. Nothing to return.
    '''
    self.file_id = data.get('file_id')

    self.duration = data.get('duration')
    self.name = data.get('name')
    self.origin = data.get('origin')
    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    '''
    save music ORM to the database.\n
    `Parameters:`\n
    \tself (MusicModel): The current music obj.
    `Return:`\n
    \t None: The function will save ORM to the database. Nothing to return.
    '''
    db.session.add(self)
    db.session.commit()
  
  def update(self, data: dict):
    '''
    update a music in the database.\n
    `Parameters:`\n
    \tself (MusicModel): The current music obj.
    \tdata (dict): dictionary of data need to update a music.
    `Return:`\n
    \t None: The function will update the database. Nothing to return.
    '''
    for key, item in data.items():
      # convert the dict to obj and update the ORM.
      setattr(self, key, item)
    # update modified_at.
    self.modified_at = datetime.datetime.utcnow()
    db.session.commit()
  
  def delete(self):
    '''
    delete a music in the database.\n
    `Parameters:`\n
    \tself (MusicModel): The current music obj.
    `Return:`\n
    \t None: The function will delete the database. Nothing to return.
    '''
    db.session.delete(self)
    db.session.commit()
  
  # static function. (function that can be call by the class instead of the obj).
  @staticmethod
  def get_all():
    return MusicModel.query.all()
  
  @staticmethod
  def get_pagination(page: int, per_page: int):
    return MusicModel.query.order_by(MusicModel.created_at.desc()).paginate(page=page, per_page=per_page)

  @staticmethod
  def get_a_music_by_id(id: Union[int, str]):
    return MusicModel.query.get(id)

  @staticmethod
  def fuzzy_search(
    page: Union[int, str],
    per_page: Union[int, str],
    # order_by: str,
    # direction: str,
    keywords: str
  ):
    return MusicModel.query.filter(
      (MusicModel.name.ilike(f'%{keywords}%')) |
      (MusicModel.origin.ilike(f'%{keywords}%'))
    ).order_by(MusicModel.created_at.desc()).paginate(page=page, per_page=per_page)

class MusicSchema(Schema):
  """
  Music Schema Class
  """
  id = fields.Int(dump_only=True)
  file_id = fields.Int(required=True, validate=validate.Range(min=0))
  artists = fields.List(fields.Nested('ArtistSchema', exclude=('musics',)))
  links = fields.List(fields.Nested('MusicLinkSchema'))
  '''
  this part will broke the request.
  fingerprints = fields.List(fields.Nested('FingerprintSchema'))
  '''

  name = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  origin = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  duration = fields.Float(required=True, validate=validate.Range(min=0))
  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)
