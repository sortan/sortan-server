import datetime
from typing import Union

from marshmallow import fields, Schema, validate

from . import db, bcrypt
from ..shared.static_variable import HttpCode
from ..shared.http_helpers import http_error_msg

CHAR_LIMIT = 512

class MusicLinkModel(db.Model):
  '''
  Music Link model.
  '''

  # assign table name.
  __tablename__ = 'music_links'

  # music table's fields.
  id = db.Column(db.Integer, primary_key=True)
  music_id = db.Column(db.Integer, db.ForeignKey('musics.id'), nullable=False)

  url = db.Column(db.String(CHAR_LIMIT), nullable=False)
  created_at = db.Column(db.DateTime)
  modified_at = db.Column(db.DateTime)

  def __init__(self, data):
    '''
    create a music link ORM.\n
    `Parameters:`\n
    \tself (MusicLinkModel): The current music link obj.
    \tdata (dict): dictionary of data need to create a music link.
    `Return:`\n
    \t None: The function will create an ORM. Nothing to return.
    '''
    self.music_id = data.get('music_id')

    self.url = data.get('url')
    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    '''
    save music link ORM to the database.\n
    `Parameters:`\n
    \tself (MusicLinkModel): The current music link obj.
    `Return:`\n
    \t None: The function will save ORM to the database. Nothing to return.
    '''
    db.session.add(self)
    db.session.commit()
  
  def update(self, data: dict):
    '''
    update a music link in the database.\n
    `Parameters:`\n
    \tself (MusicLinkModel): The current music link obj.
    \tdata (dict): dictionary of data need to update a music link.
    `Return:`\n
    \t None: The function will update the database. Nothing to return.
    '''
    for key, item in data.items():
      # convert the dict to obj and update the ORM.
      setattr(self, key, item)
    # update modified_at.
    self.modified_at = datetime.datetime.utcnow()
    db.session.commit()
  
  def delete(self):
    '''
    delete a music link in the database.\n
    `Parameters:`\n
    \tself (MusicLinkModel): The current music link obj.
    `Return:`\n
    \t None: The function will delete the database. Nothing to return.
    '''
    db.session.delete(self)
    db.session.commit()
  
  # static function. (function that can be call by the class instead of the obj).
  @staticmethod
  def get_all():
    return MusicLinkModel.query.all()
  
  @staticmethod
  def get_pagination(page: int, per_page: int):
    return MusicLinkModel.query.order_by(MusicLinkModel.created_at.desc()).paginate(page=page, per_page=per_page)

  @staticmethod
  def get_a_music_by_id(id: Union[int, str]):
    return MusicLinkModel.query.get(id)

  @staticmethod
  def fuzzy_search(
    page: Union[int, str],
    per_page: Union[int, str],
    # order_by: str,
    # direction: str,
    keywords: str
  ):
    return MusicLinkModel.query.filter(
      (MusicLinkModel.url.ilike(f'%{keywords}%'))
    ).order_by(MusicLinkModel.created_at.desc()).paginate(page=page, per_page=per_page)

class MusicLinkSchema(Schema):
  """
  Music Schema Class
  """
  id = fields.Int(dump_only=True)
  music_id = fields.Int(required=True, validate=validate.Range(min=0))

  url = fields.URL(required=True, validate=validate.Length(max=CHAR_LIMIT))
  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)
