import datetime
import os
from typing import Union

from marshmallow import fields, Schema, validate

from . import db, bcrypt
from ..shared.static_variable import HttpCode
from ..shared.http_helpers import http_error_msg

CHAR_LIMIT = 256

artist_music_association_table = db.Table(
  'artist_music_association',
  db.Column('artist_id', db.Integer, db.ForeignKey('artists.id'), primary_key=True),
  db.Column('music_id', db.Integer, db.ForeignKey('musics.id'), primary_key=True)
)

class ArtistModel(db.Model):
  '''
  Artist model.
  '''

  # assign table name.
  __tablename__ = 'artists'

  # artists table's fields.
  id = db.Column(db.Integer, primary_key=True)
  musics = db.relationship(
    'MusicModel',
    secondary=artist_music_association_table,
    backref='artists',
    uselist=True,
    lazy=True,
    cascade='all,delete'
  )
  
  name = db.Column(db.String(CHAR_LIMIT), nullable=False)
  created_at = db.Column(db.DateTime)
  modified_at = db.Column(db.DateTime)

  def __init__(self, data):
    '''
    create a artist ORM.\n
    `Parameters:`\n
    \tself (ArtistModel): The current artist obj.
    \tdata (dict): dictionary of data need to create a artist.
    `Return:`\n
    \t None: The function will create an ORM. Nothing to return.
    '''
    self.name = data.get('name')
    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    '''
    save artist ORM to the database.\n
    `Parameters:`\n
    \tself (ArtistModel): The current artist obj.
    `Return:`\n
    \t None: The function will save ORM to the database. Nothing to return.
    '''
    db.session.add(self)
    db.session.commit()
  
  def update(self, data):
    for key, item in data.items():
      setattr(self, key, item)
    self.modified_at = datetime.datetime.utcnow()
    db.session.commit()
  
  def delete(self):
    '''
    delete a artist in the database.\n
    `Parameters:`\n
    \tself (ArtistModel): The current artist obj.
    `Return:`\n
    \t None: The function will delete the database. Nothing to return.
    '''
    # remove the artist from db.
    db.session.delete(self)
    db.session.commit()
  
  # static function. (function that can be call by the class instead of the obj).

  @staticmethod
  def get_all():
    return ArtistModel.query.all()
  
  @staticmethod
  def get_pagination(page: int, per_page: int):
    return ArtistModel.query.order_by(ArtistModel.created_at.desc()).paginate(page=page, per_page=per_page)
  
  @staticmethod
  def get_an_artist_by_id(id: Union[int, str]):
    return ArtistModel.query.get(id)

  # Need get artist by music_id.
  
  @staticmethod
  def get_an_artist_by_name(name: str):
    return ArtistModel.query.filter_by(name=name).first()

  @staticmethod
  def fuzzy_search(
    page: Union[int, str],
    per_page: Union[int, str],
    # order_by: str,
    # direction: str,
    keywords: str
  ):
    return ArtistModel.query.filter(
      (ArtistModel.name.ilike(f'%{keywords}%'))
    ).order_by(ArtistModel.created_at.desc()).paginate(page=page, per_page=per_page)

class ArtistSchema(Schema):
  """
  Artist Schema Class
  """
  id = fields.Int(dump_only=True)
  musics = fields.List(fields.Nested('MusicSchema', exclude=('artists',)))

  name = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)
