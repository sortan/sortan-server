import datetime
import os
from typing import Union

from marshmallow import fields, Schema, validate

from . import db, bcrypt, UPLOAD_FOLDER, ALLOW_FILE_EXTENSION
from ..shared.static_variable import HttpCode
from ..shared.http_helpers import http_error_msg
from .MusicModel import MusicSchema

CHAR_LIMIT = 256

class FileModel(db.Model):
  '''
  File model.
  '''

  # assign table name.
  __tablename__ = 'files'

  # files table's fields.
  id = db.Column(db.Integer, primary_key=True)
  music = db.relationship('MusicModel', backref='file', uselist=False, lazy=True, cascade="all,delete,delete-orphan")

  size = db.Column(db.Integer, nullable=False)
  path = db.Column(db.String(CHAR_LIMIT), unique=True, nullable=False)
  type = db.Column(db.String(CHAR_LIMIT), nullable=False)
  created_at = db.Column(db.DateTime)
  modified_at = db.Column(db.DateTime)

  def __init__(self, data):
    '''
    create a file ORM.\n
    `Parameters:`\n
    \tself (FileModel): The current file obj.
    \tdata (dict): dictionary of data need to create a file.
    `Return:`\n
    \t None: The function will create an ORM. Nothing to return.
    '''
    self.size = data.get('size')
    self.path = data.get('path')
    self.type = data.get('type')
    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    '''
    save file ORM to the database.\n
    `Parameters:`\n
    \tself (FileModel): The current file obj.
    `Return:`\n
    \t None: The function will save ORM to the database. Nothing to return.
    '''
    db.session.add(self)
    db.session.commit()
  
  def delete(self):
    '''
    delete a file in the database.\n
    `Parameters:`\n
    \tself (FileModel): The current file obj.
    `Return:`\n
    \t None: The function will delete the database. Nothing to return.
    '''
    # delete the file in the file system.
    file_path = os.path.join(UPLOAD_FOLDER, self.path)
    if os.path.exists(file_path):
      os.remove(file_path)
    # remove the file from db.
    db.session.delete(self)
    db.session.commit()
  
  # static function. (function that can be call by the class instead of the obj).
  @staticmethod
  def is_file_supported(filename: str):
    extension = filename.split('.')[-1]
    return extension in ALLOW_FILE_EXTENSION

  @staticmethod
  def get_all():
    return FileModel.query.all()
  
  @staticmethod
  def get_pagination(page: int, per_page: int):
    return FileModel.query.order_by(FileModel.created_at.desc()).paginate(page=page, per_page=per_page)
  
  @staticmethod
  def get_a_file_by_id(id: Union[int, str]):
    return FileModel.query.get(id)
  
  @staticmethod
  def get_a_file_by_path(path: str):
    return FileModel.query.filter_by(path=path).first()

  @staticmethod
  def fuzzy_search(
    page: Union[int, str],
    per_page: Union[int, str],
    # order_by: str,
    # direction: str,
    keywords: str
  ):
    return FileModel.query.filter(
      (FileModel.path.ilike(f'%{keywords}%'))
    ).order_by(FileModel.created_at.desc()).paginate(page=page, per_page=per_page)

class FileSchema(Schema):
  """
  File Schema Class
  """
  id = fields.Int(dump_only=True)
  music = fields.Nested(MusicSchema, exclude=('file_id',))

  size = fields.Int(required=True, validate=validate.Range(min=1))
  path = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  type = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)

def file_get_full_path(file_obj):
  path = os.path.join(UPLOAD_FOLDER, file_obj.get('path'))
  if path == None:
    raise RuntimeError('Error, the file path not found')
  
  return path
