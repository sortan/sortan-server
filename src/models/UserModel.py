import datetime
from typing import Union

from marshmallow import fields, Schema, validate

from . import db, bcrypt
from ..shared.static_variable import HttpCode
from ..shared.http_helpers import http_error_msg

CHAR_LIMIT = 256

class UserModel(db.Model):
  '''
  User model.
  '''

  # assign table name.
  __tablename__ = 'users'

  # users table's fields.
  id= db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(CHAR_LIMIT), nullable=False)
  email = db.Column(db.String(CHAR_LIMIT), unique=True, nullable=False)
  password = db.Column(db.String(CHAR_LIMIT), nullable=False)

  def __init__(self, data):
    '''
    create a user ORM.\n
    `Parameters:`\n
    \tself (UserModel): The current user obj.
    \tdata (dict): dictionary of data need to create a user.
    `Return:`\n
    \t None: The function will create an ORM. Nothing to return.
    '''
    self.name = data.get('name')
    self.email = data.get('email')
    self.password = self.__generate_hash(data.get('password'))
    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    '''
    save user ORM to the database.\n
    `Parameters:`\n
    \tself (UserModel): The current user obj.
    `Return:`\n
    \t None: The function will save ORM to the database. Nothing to return.
    '''
    db.session.add(self)
    db.session.commit()
  
  def update(self, data: dict):
    '''
    update a user in the database.\n
    `Parameters:`\n
    \tself (UserModel): The current user obj.
    \tdata (dict): dictionary of data need to update a user.
    `Return:`\n
    \t None: The function will update the database. Nothing to return.
    '''
    for key, item in data.items():
      if key == 'password':
        raise RuntimeError(http_error_msg(
          'Password are not allow to change in this route',
          HttpCode.BAD_REQUEST
        ))
      # convert the dict to obj and update the ORM.
      setattr(self, key, item)
    # update modified_at.
    self.modified_at = datetime.datetime.utcnow()
    db.session.commit()
  
  def delete(self):
    '''
    delete a user in the database.\n
    `Parameters:`\n
    \tself (UserModel): The current user obj.
    `Return:`\n
    \t None: The function will delete the database. Nothing to return.
    '''
    db.session.delete(self)
    db.session.commit()
  
  # static function. (function that can be call by the class instead of the obj).
  @staticmethod
  def get_all():
    return UserModel.query.all()
  
  @staticmethod
  def get_pagination(page: int, per_page: int, order_by: str, direction: str):
    return UserModel.query.paginate(page=page, per_page=per_page).order_by(f'{order_by} {direction}')
  
  @staticmethod
  def get_a_user_by_id(id: Union[int, str]):
    return UserModel.query.get(id)
  
  @staticmethod
  def get_a_user_by_email(email: str):
    return UserModel.query.filter_by(email=email).first()

  @staticmethod
  def fuzzy_search(
    page: Union[int, str],
    per_page: Union[int, str],
    order_by: str,
    direction: str,
    keywords: str
  ):
    return UserModel.query.filter(
      (UserModel.name.ilike(f'%{keywords}%')) |
      (UserModel.email.ilike(f'%{keywords}%'))
    ).paginate(page=page, per_page=per_page).order_by(f'{order_by} {direction}')

  # protected functions.
  def __generate_hash(self, password):
    '''
    generate a hash for the user password.\n
    `Parameters:`\n
    \tself (UserModel): The current user obj.
    \tpassword (str): the user password.
    `Return:`\n
    \thash-pwd (str): the hash password of the user.
    '''
    return bcrypt.generate_password_hash(password, rounds=10).decode("utf-8")
  
  def compare_password(self, password: str):
    return bcrypt.check_password_hash(self.password, password)

class UserSchema(Schema):
  """
  User Schema Class
  """
  id = fields.Int(dump_only=True)
  
  name = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  email = fields.Email(required=True, validate=validate.Length(max=CHAR_LIMIT))
  password = fields.Str(required=True, validate=validate.Length(max=CHAR_LIMIT))
  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)
