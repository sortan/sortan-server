from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt

UPLOAD_FOLDER = 'files/upload'
ALLOW_FILE_EXTENSION = ('wav',)

db = SQLAlchemy()
bcrypt = Bcrypt()

# import each model and schema
from .UserModel import UserModel, UserSchema
from .FileModel import FileModel, FileSchema
from .MusicModel import MusicModel, MusicSchema
from .ArtistModel import ArtistModel, ArtistSchema
from .FingerprintModel import FingerprintModel, FingerprintSchema
from .MusicLinkModel import MusicLinkModel, MusicLinkSchema
