import datetime
import os
from typing import Union

import pandas as pd
import random

from marshmallow import fields, Schema, validate
from sqlalchemy import tuple_, func

from . import db, bcrypt
from ..shared.static_variable import HttpCode
from ..shared.http_helpers import http_error_msg

CHAR_LIMIT = 256

class FingerprintModel(db.Model):
  '''
  Fingerprint model.
  '''

  # assign table name.
  __tablename__ = 'fingerprints'

  # fingerprints table's fields.
  id = db.Column(db.BigInteger, primary_key=True)
  music_id = db.Column(db.Integer, db.ForeignKey('musics.id'), nullable=False)

  anchor_frequency = db.Column(db.Integer, nullable=False)
  anchor_time = db.Column(db.Integer, nullable=False)
  point_frequency = db.Column(db.Integer, nullable=False)
  point_time = db.Column(db.Integer, nullable=False)
  time_difference = db.Column(db.Integer, nullable=False)

  lookup_id = db.Column(db.BigInteger, nullable=False)

  created_at = db.Column(db.DateTime)
  modified_at = db.Column(db.DateTime)

  def __init__(self, data):
    '''
    create a fingerprint ORM.\n
    `Parameters:`\n
    \tself (FingerprintModel): The current fingerprint obj.
    \tdata (dict): dictionary of data need to create a fingerprint.
    `Return:`\n
    \t None: The function will create an ORM. Nothing to return.
    '''
    self.music_id = data.get('music_id')
    
    self.anchor_frequency = data.get('anchor_frequency')
    self.anchor_time = data.get('anchor_time')
    self.point_frequency = data.get('point_frequency')
    self.point_time = data.get('point_time')
    self.time_difference = data.get('time_difference')

    self.lookup_id = int(f'{self.anchor_frequency}0{self.point_frequency}0{self.time_difference}')

    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    '''
    save fingerprint ORM to the database.\n
    `Parameters:`\n
    \tself (FingerprintModel): The current fingerprint obj.
    `Return:`\n
    \t None: The function will save ORM to the database. Nothing to return.
    '''
    db.session.add(self)
    db.session.commit()
  
  def update(self, data):
    for key, item in data.items():
      setattr(self, key, item)
    self.modified_at = datetime.datetime.utcnow()
    db.session.commit()
  
  def delete(self):
    '''
    delete a fingerprint in the database.\n
    `Parameters:`\n
    \tself (FingerprintModel): The current fingerprint obj.
    `Return:`\n
    \t None: The function will delete the database. Nothing to return.
    '''
    # remove the fingerprint from db.
    db.session.delete(self)
    db.session.commit()
  
  # static function. (function that can be call by the class instead of the obj).
  @staticmethod
  def save_all(data):
    db.session.add_all(data)
    db.session.commit()

  @staticmethod
  def get_all():
    return FingerprintModel.query.all()
  
  @staticmethod
  def get_pagination(page: int, per_page: int):
    return FingerprintModel.query.order_by(FingerprintModel.created_at.desc()).paginate(page=page, per_page=per_page)
  
  @staticmethod
  def get_an_artist_by_id(id: Union[int, str]):
    return FingerprintModel.query.get(id)
  
  @staticmethod
  def search(data):
    '''
    search for fingerprint
    '''

    '''
      search using the and tuple

      return FingerprintModel.query.filter(
        tuple_(
          FingerprintModel.anchor_frequency,
          FingerprintModel.point_frequency,
          FingerprintModel.time_difference
        )
        .in_(data)).all()
    '''

    data = set(data)
    temp_table = f'temp{random.randint(0, 1000)}'
    values = ','.join(f'({lookup_id})' for lookup_id in data)
    df =  pd.read_sql(
      f"CREATE TEMP TABLE {temp_table} AS SELECT * FROM (VALUES{values}) x(lookup_id); SELECT fingerprints.music_id, fingerprints.lookup_id, fingerprints.anchor_time FROM fingerprints INNER JOIN {temp_table} USING(lookup_id)", 
      db.session.bind
    )

    db.session.execute(f"DROP TABLE IF EXISTS {temp_table}")
    return df

    '''
    # using ORM take 20 second for a 10 second clip.
    return pd.read_sql(
      FingerprintModel.query.with_entities(
        FingerprintModel.music_id,
        FingerprintModel.lookup_id,
        FingerprintModel.anchor_time
      ).filter(
        FingerprintModel.lookup_id.in_(data)
      ).statement, 
      db.session.bind
    )
    '''

    '''
    This is another search option:
      conditions = (and_(
        FingerprintModel.anchor_frequency==x,
        FingerprintModel.point_frequency==y,
        FingerprintModel.time_difference==z) for (x, y, z) in data)
      return db.session.query(FingerprintModel).filter(or_(*conditions)).all()
    '''

  @staticmethod
  def mini_search(anchor_frequency, point_frequency, time_difference):
    q = FingerprintModel.query.filter_by(
      anchor_frequency=anchor_frequency, 
      point_frequency=point_frequency, 
      time_difference=time_difference)
    return q.all()
class FingerprintSchema(Schema):
  """
  Finger Schema Class
  """
  id = fields.Int(dump_only=True)
  music_id = fields.Int(required=True, validate=validate.Range(min=0))

  anchor_frequency = fields.Integer(required=True, validate=validate.Range(min=0))
  anchor_time = fields.Integer(required=True, validate=validate.Range(min=0))
  point_frequency = fields.Integer(required=True, validate=validate.Range(min=0))
  point_time = fields.Integer(required=True, validate=validate.Range(min=0))
  time_difference = fields.Integer(required=True, validate=validate.Range(min=0))

  lookup_id = fields.Integer(dump_only=True)

  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)
