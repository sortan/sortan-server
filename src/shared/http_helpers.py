from flask import Response, json
from typing import Union

def http_error_msg(message: str, code: int) -> dict:
  return {
    'response': { 'message': message },
    'code': code
  }

def http_msg_constructor(message: Union[str, dict], success=False) -> dict:
  return {
    'message': message,
    'success': success
  }

def response_helper(res: dict, status_code: int):
  """
  Custom Response Helper Function
  """
  return Response(
    mimetype="application/json",
    response=json.dumps(res),
    status=status_code
  )
