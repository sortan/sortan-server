import os
import datetime
from functools import wraps
from typing import Union

import jwt
from flask import g, request, json, Response

from ..models.UserModel import UserModel
from .http_helpers import response_helper, http_msg_constructor
from .static_variable import HttpCode

mimetype = "application/json"

class Auth:
  '''
  Class for dealing with authentication and authoraization.
  '''
  @staticmethod
  def generate_token(user_id: int):
    '''
    caller need to use `try` Exception.
    '''
    payload = {
      'exp': datetime.datetime.utcnow() + datetime.timedelta(days=30),
      'iat': datetime.datetime.utcnow(),
      'sub': user_id
    }
    return jwt.encode(
      payload,
      os.getenv('JWT_SECRET_KEY'),
      'HS256'
    )
  
  @staticmethod
  def decode_token(token: Union[str, bytes]):
    '''
    caller need to `try` jwt.ExpiredSignatureError and jwt.InvalidTokenError
    '''
    payload = jwt.decode(token, os.getenv('JWT_SECRET_KEY'))
    return payload['sub']
  
  @classmethod
  def authenticate(cls, func):
    '''
    authentication decorator. It purpose is to authenticate the request.
    '''
    @wraps(func)
    def decorated_authentication(*args, **kwargs):
      # declare variable.
      valid_prefix = 'Bearer'
      # check for Authorization header.
      if 'Authorization' not in request.headers:
        return response_helper(http_msg_constructor('Authorization not found!'), HttpCode.BAD_REQUEST)
      # get the Authorization header contents.
      prefix, token = request.headers.get('Authorization').split()

      # sanitize check prefix and token.
      if not prefix or not token:
        return response_helper(http_msg_constructor('Auth token not found!'), HttpCode.BAD_REQUEST)
      if prefix != valid_prefix:
        return response_helper(http_msg_constructor('Prefix does not match!'), HttpCode.BAD_REQUEST)
      
      # decode the token.
      try:
        user_id = Auth.decode_token(token)
      except jwt.ExpiredSignatureError:
        return response_helper(http_msg_constructor('Token expired, please login again!'), HttpCode.BAD_REQUEST)
      except jwt.InvalidTokenError:
        return response_helper(http_msg_constructor('Invalid token, please try again with a new token!'), HttpCode.BAD_REQUEST)

      # check if the user exist or not.
      try:
        user = UserModel.get_a_user_by_id(user_id)
        if not user:
          return response_helper(http_msg_constructor('User not found'), HttpCode.BAD_REQUEST)
      except Exception as err:
        return response_helper(err, HttpCode.INTERNAL_SERVER_ERROR)

      # add dict to flask global `g`.
      g.user = user

      # put the original function here.
      return func(*args, **kwargs)

    # return the decorated function.
    return decorated_authentication



