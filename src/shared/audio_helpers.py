from typing import Union
from pydub import AudioSegment
import numpy as np
import statistics
from scipy.signal import spectrogram
from .static_variable import VALID_MAX_FREQUENCY, VALID_SAMPLE_RATE, MONO

from ..models.FingerprintModel import FingerprintModel, FingerprintSchema

def load_wav(file_path: str):
	# load as AudioSegment
	return AudioSegment.from_wav(file_path)

def stereo_to_mono(audio: AudioSegment):
	if audio.channels == 1:
		return audio
	elif audio.channels == 2:
		return audio.set_channels(1)
	else:
		raise ValueError('audio channel can only be 1 or 2!')

def is_audio_mono(audio: AudioSegment):
  return audio.channels == 1

def is_sample_rate_valid(sample_rate):
  return sample_rate == VALID_SAMPLE_RATE

# Might not use.
def low_pass_filter(audio: AudioSegment, cut_off: int) -> AudioSegment:
	return audio.low_pass_filter(cut_off)

def downsample(audio: AudioSegment, frame_rate: int):
	return audio.set_frame_rate(frame_rate)

def get_wav_info(audio: AudioSegment):
	# convert the pydub samples array to numpy array for later uses.
	return np.array(audio.get_array_of_samples()), audio.frame_rate

def get_max_amplitude_between_freq(time_frame: np.ndarray, min_freq: int, max_freq: int):
  amplitudes = time_frame[min_freq : max_freq]
  if amplitudes.size > 0:
    return amplitudes.max()
  else:
    return 0


def gen_highest_peaks(
  frames: np.ndarray,
  frame_rate: float,
  window_size: int,
  overlap: int
):
  # get spectrum, frequency, time from spectrogram. 
  # spectrum have column as frequency and row as time.
  frequencies, times, spectrum = spectrogram(
    frames,
    fs=frame_rate,
    # as defined in the paper we going to use 1024 bin.
    window = np.hamming(window_size),
    nfft = window_size,
    # in the recent article, it mention to use the spectrum magnitude.
    # mode='magnitude',
    # need to add overlap 0 too to get what the paper wanted.
    noverlap = overlap,
  )

  '''
  for some reason the frequency length is 513, it should be 512 because it is
  Number-of-Bin = Window-size / 2 in this case it is 1024 / 2.
  so here are the step to generate the contellation map:
    + for each time unit (0.1s) we group the frequency into 6 band:
      - the very low sound band (from freq bin 0 to 10)
      - the low sound band (from freq bin 10 to 20)
      - the low-mid sound band (from freq bin 20 to 40)
      - the mid sound band (from freq bin 40 to 80)
      - the mid-high sound band (from freq bin 80 to 160)
      - the high sound band (from freq bin 160 to 511)
    + keep the strongest amplitude from each band.
    + calculate the mean of the strongest amplitude.
    + 1 if strongest-amplitude > mean else 0
  '''
  # create new numpy array with the same dimension of spectrum. for PLOTTING purpose only.
  highest_peaks = []

  # get each time frame.
  '''
  TODO: DEBUG this shit. (DONE)
  - write a helper function that get the max value from each band.
  If the the band is empty return 0.
  - when the qualify_band_idexes max value is 0, then no (time, frequency) pair need to be added.
  - when qualify_band_idexes is empty, then no (time, frequency) pair need to be added.

  '''
  for index in range(times.size):
    time_frame = spectrum[:, index]
    # time = times[index]

    # get the the 6 bands of time frame.
    vl_band = get_max_amplitude_between_freq(time_frame, 0, 40)
    l_band = get_max_amplitude_between_freq(time_frame, 40, 80)
    lm_band = get_max_amplitude_between_freq(time_frame, 80, 160)
    m_band = get_max_amplitude_between_freq(time_frame, 160, 320)
    mh_band = get_max_amplitude_between_freq(time_frame, 320, 640)
    h_band = get_max_amplitude_between_freq(time_frame, 640, 2049)
    # calculate the amplitude mean of the time frame.
    bands = np.array([
      vl_band,
      l_band,
      lm_band,
      m_band,
      mh_band,
      h_band
    ])
    if bands.max() > 0:
      mean_amplitude = statistics.mean(bands)
      '''
      find the index of band that the amplitude is higher then the mean.
      in the paper, the mean is multiply by a coefficient.
      it is also mention that this particular algorithm is not very efficient.
      it can be change to `logarithmic sliding window and to keep only the most powerful
      frequencies above the mean + the standard deviation (multiplied by a coefficient) of
      a moving part of the song.`
      '''
      qualify_band_idexes = np.where(bands > mean_amplitude)[0]
      # get the qualify amplitude in the time frame.
      qualify_amplitudes = bands[qualify_band_idexes]
      # get the frequency indexes in the time frame that will product the qualify amplitude.

      # high_peaks_freq_idexes = np.sort(np.nonzero(np.in1d(time_frame, qualify_amplitudes))[0])
      high_peaks_freq_idexes = np.sort(np.nonzero(np.in1d(time_frame, bands))[0])

      '''
      Before the grouping into target zone, we need to sort the our time-frequency pair first.
      - For each time-frame we it is sorted by low freq to high freq.
      - For each spectrogram, we sorted it by lowest-time to highest-time.
      '''
      # get frequenies by index in the time frame and sort it.
      # high_peaks_freq = np.sort(frequencies[high_peaks_freq_idexes])

      # loop through those index and create a coordinate for it (time, frequency)
      for freq in high_peaks_freq_idexes:
        highest_peaks.append({
          # 'time': time,
          'time': index,
          'frequency': freq
        })

  return highest_peaks

def generate_fingerprints(
  frames: np.ndarray,
  frame_rate: float,
  window_size: int,
  overlap: int,
  music_id: Union[int, str]
):
  highest_peaks = gen_highest_peaks(frames, frame_rate, window_size, overlap)

  '''Grouping target zone'''
  fingerprints = []
  ZONE_LENGTH = 5
  ANCHOR_LEN_DIFF = 3
  # FingerprintSchema init.
  fingerprint_schema = FingerprintSchema()
  # loop through highest_peaks and create target-zone
  # we loop through the index instead because we want to look ahead and add them to the same
  # target-zone. And this case we remove `4` from the length so that the 4 index won't form
  # any out of range target-zone.
  for peaks_index in range(len(highest_peaks) - (ZONE_LENGTH - 1)):
    # get the anchor point for this particular zone.
    anchor_point = highest_peaks[peaks_index - ANCHOR_LEN_DIFF] if peaks_index > ANCHOR_LEN_DIFF else highest_peaks[0]
    # create the fingerprint for each point in the zone.
    for i in range(ZONE_LENGTH):
      current_point = highest_peaks[peaks_index + i]
      fingerprints.append(FingerprintModel(fingerprint_schema.load({
        'music_id': music_id,
        'anchor_frequency': anchor_point['frequency'],
        'anchor_time': anchor_point['time'],
        'point_frequency': current_point['frequency'],
        'point_time': current_point['time'],
        'time_difference': current_point['time'] - anchor_point['time']
      })))

  return fingerprints

def generate_searchable_data(
  frames: np.ndarray,
  frame_rate: float,
  window_size: int,
  overlap: int,
):
  highest_peaks = gen_highest_peaks(frames, frame_rate, window_size, overlap)

  '''Grouping target zone'''
  addresses = []
  anchor_times = []
  lookup_ids = []
  ZONE_LENGTH = 5
  ANCHOR_LEN_DIFF = 3
  # FingerprintSchema init.
  fingerprint_schema = FingerprintSchema(exclude=('music_id',))
  # loop through highest_peaks and create target-zone
  # we loop through the index instead because we want to look ahead and add them to the same
  # target-zone. And this case we remove `4` from the length so that the 4 index won't form
  # any out of range target-zone.
  highest_peaks_len = len(highest_peaks)
  for peaks_index in range(highest_peaks_len - (ZONE_LENGTH - 1)):
    # get the anchor point for this particular zone.
    anchor_point = highest_peaks[peaks_index - ANCHOR_LEN_DIFF] if peaks_index > ANCHOR_LEN_DIFF else highest_peaks[0]
    # create the fingerprint for each point in the zone.
    for i in range(ZONE_LENGTH):
      current_point = highest_peaks[peaks_index + i]
      fingerprint = fingerprint_schema.load({
        'anchor_frequency': anchor_point['frequency'],
        'anchor_time': anchor_point['time'],
        'point_frequency': current_point['frequency'],
        'point_time': current_point['time'],
        'time_difference': current_point['time'] - anchor_point['time']
      })
      # addresses.append((
      #   fingerprint['anchor_frequency'],
      #   fingerprint['point_frequency'],
      #   fingerprint['time_difference']
      # ))
      anchor_times.append(fingerprint['anchor_time'])
      lookup_ids.append(int(f"{fingerprint['anchor_frequency']}0{fingerprint['point_frequency']}0{fingerprint['time_difference']}"))

  # highest_peaks_len is the approximate total_target_zones we have.
  # return addresses, anchor_times, (highest_peaks_len - (ZONE_LENGTH - 1)), lookup_ids
  return anchor_times, (highest_peaks_len - (ZONE_LENGTH - 1)), lookup_ids
