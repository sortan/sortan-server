from flask import Flask
from .config import app_config
from .models import db, bcrypt, UPLOAD_FOLDER

# import blueprint for routing in other directory.
from .views.UserView import user_api as user_blueprint
from .views.FileView import file_api as file_blueprint
from .views.MusicView import music_api as music_blueprint
from .views.ArtistView import artist_api as artist_blueprint
from .views.MusicLinkView import music_links_api as music_link_blueprint

def create_app(env_name):
  """
  Create the app
  """

  # app initialization
  app = Flask(__name__)
  app.config.from_object(app_config[env_name])

  # config the file storage.
  app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

  # initialize bycrypt and db.
  bcrypt.init_app(app)
  db.init_app(app)

  # initialize the route.
  app.register_blueprint(user_blueprint, url_prefix='/users')
  app.register_blueprint(file_blueprint, url_prefix='/files')
  app.register_blueprint(music_blueprint, url_prefix='/musics')
  app.register_blueprint(artist_blueprint, url_prefix='/artists')
  app.register_blueprint(music_link_blueprint, url_prefix='/music_links')

  @app.route('/', methods=['GET'])
  def index():
    """
    example endpoint
    """
    return 'Congratulations! Your first endpoint is working'

  return app
