from gevent.pywsgi import WSGIServer
from src.app import create_app
import os

if __name__ == '__main__':
    env_name = os.getenv("FLASK_ENV")
    app = create_app(env_name)
    http_server = WSGIServer(('', 5000), app)
    http_server.serve_forever()
